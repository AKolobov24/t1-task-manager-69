package ru.t1.akolobov.tm.web;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.t1.akolobov.tm.web.configuration.ApplicationConfiguration;
import ru.t1.akolobov.tm.web.configuration.DatabaseConfiguration;
import ru.t1.akolobov.tm.web.configuration.WebApplicationConfiguration;
import ru.t1.akolobov.tm.web.configuration.WebServiceConfiguration;

import javax.servlet.ServletContext;

public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{ApplicationConfiguration.class, DatabaseConfiguration.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{WebApplicationConfiguration.class, WebServiceConfiguration.class};
    }

    @NotNull
    @Override
    protected String @NotNull [] getServletMappings() {
        return new String[]{"/"};
    }

    @Override
    protected void registerContextLoaderListener(@NotNull final ServletContext servletContext) {
        super.registerContextLoaderListener(servletContext);
    }

}
