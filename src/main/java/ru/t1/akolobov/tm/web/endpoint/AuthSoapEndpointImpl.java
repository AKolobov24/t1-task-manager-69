package ru.t1.akolobov.tm.web.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.akolobov.tm.web.dto.soap.auth.LoginRequest;
import ru.t1.akolobov.tm.web.dto.soap.auth.LoginResponse;
import ru.t1.akolobov.tm.web.dto.soap.auth.LogoutRequest;
import ru.t1.akolobov.tm.web.dto.soap.auth.LogoutResponse;

@Endpoint
public final class AuthSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";
    public final static String PORT_TYPE_NAME = "AuthSoapEndpointPort";
    public final static String NAMESPACE = "http://t1.ru/akolobov/tm/web/dto/soap/auth";

    @Autowired
    private AuthenticationManager authenticationManager;


    @ResponsePayload
    @PayloadRoot(localPart = "loginRequest", namespace = NAMESPACE)
    public LoginResponse login(
            @NotNull
            @RequestPayload final LoginRequest request
    ) {
        @NotNull final LoginResponse response = new LoginResponse();
        try {
            final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword());
            final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            response.setSuccess(true);
            return response;
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage(e.getMessage());
            return response;
        }
    }

    @PayloadRoot(localPart = "logoutRequest", namespace = NAMESPACE)
    @ResponsePayload
    public LogoutResponse logout(
            @NotNull
            @RequestPayload final LogoutRequest request
    ) {
        @NotNull final LogoutResponse response = new LogoutResponse();
        SecurityContextHolder.getContext().setAuthentication(null);
        response.setSuccess(true);
        return response;
    }

}
