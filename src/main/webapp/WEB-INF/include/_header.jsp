<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>TASK MANAGER</title>
    </head>
    <body>
        <table width="100%" height="100%" cellpadding="10" border="1" style="border-collapse: collapse; ">
            <tr height="35">
                <td width="50%">
                    <b>TASK MANAGER</b>
                </td>
                <td width="35%" align="right">
                    <a href="/projects">PROJECTS</a>
                        |
                    <a href="/tasks">TASKS</a>
                </td>
                <sec:authorize access = "isAuthenticated()">
                    <td width="15%" align="right">
                        logged in as: <b><sec:authentication property="name" /></b>
                            |
                        <a href="/logout">logout</a>
                    </td>
                </sec:authorize>
            </tr>
            <tr>
                <td colspan="3" style="vertical-align: top;">
