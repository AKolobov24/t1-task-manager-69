<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp" />

<form name='f' action='/auth' method='POST'>
    <table width="400" cellpadding="10" border="1" align="center" style="border-collapse: collapse;">
        <tr>
            <td colspan='2' align="center">
                <b>Login with Username and Password</b>
            </td>
        </tr>
        <tr>
            <td>Username:</td>
            <td><input type='text' name='username' value=''></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><input type='password' name='password'/></td>
        </tr>
        <tr>
            <td colspan='2' align="center"><input name="submit" type="submit" value="Login"/></td>
        </tr>
    </table>
</form>
<jsp:include page="../include/_footer.jsp" />
